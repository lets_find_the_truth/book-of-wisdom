//
//  QuotesCategoriesCollectionViewCell.swift
//  Great Sayings
//
//  Created by Kyryl Nevedrov on 22/04/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class QuotesCategoriesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var tileImageView: UIImageView!
    @IBOutlet weak var categoryTitleLabel: UILabel!
    @IBOutlet weak var quoteLabel: UILabel!
}
