//
//  QuotesCategoriesViewController.swift
//  Great Sayings
//
//  Created by Kyryl Nevedrov on 19/04/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class QuotesCategoriesViewController: UIViewController {
    @IBOutlet weak var collectionViewFlowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK - variables
    private var disposeBag = DisposeBag()
    var viewModel: QuotesCategoriesViewModel = QuotesCategoriesViewModel(quotesService: QuotesService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        bindUI()
    }
    
    private func setupUI() {
        let screenSize = UIScreen.main.bounds
        let space: CGFloat = 10
        let cellHeight = screenSize.height / 1.5
        let cellWidth = screenSize.width - space * 5 * 2
        collectionViewFlowLayout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        collectionViewFlowLayout.minimumLineSpacing = space
        collectionViewFlowLayout.minimumInteritemSpacing = space
        collectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 0, left: 40, bottom: 0, right: space * 5)
    }
    
    private func bindUI() {
        viewModel.categories.bind(to: collectionView.rx.items(cellIdentifier: "QuotesCategoriesCollectionViewCell", cellType: QuotesCategoriesCollectionViewCell.self)) { (row, model, cell) in
                cell.categoryTitleLabel.text = model.title
                cell.quoteLabel.text = model.quote
                cell.tileImageView.image = UIImage(named: "\(model.imageName)")
            }
            .disposed(by: disposeBag)
        
        collectionView.rx.modelSelected(Category.self)
            .subscribe(onNext: { [weak self] (category) in
                let navController = self?.navigationController
                let quotesListViewController = UIStoryboard(name: "Main", bundle: .main).initFromStoryboard(viewController: QuotesListViewController.self)
                quotesListViewController.viewModel = QuotesListViewModel(category: category)
                navController?.pushViewController(quotesListViewController, animated: true)
            })
            .disposed(by: disposeBag)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension QuotesCategoriesViewController: UIScrollViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let itemSizeWithSpace = collectionViewFlowLayout.itemSize.width + collectionViewFlowLayout.minimumInteritemSpacing
        let offset = targetContentOffset.pointee
        let index = (offset.x + scrollView.contentInset.left) / itemSizeWithSpace
        let roundedIndex = round(index)
        targetContentOffset.pointee = CGPoint(x: roundedIndex * itemSizeWithSpace + scrollView.contentInset.left - collectionViewFlowLayout.minimumInteritemSpacing, y: 0)
    }
    
    
}
