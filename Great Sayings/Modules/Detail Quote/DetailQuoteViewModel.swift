//
//  DetailQuoteViewModel.swift
//  Great Sayings
//
//  Created by Kyryl Nevedrov on 19/04/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import RxSwift

class DetailQuoteViewModel {
    var quote: Observable<Quote>
    
    init(quote: Quote) {
        self.quote = Observable.of(quote)
    }
}
