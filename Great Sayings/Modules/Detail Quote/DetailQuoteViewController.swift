//
//  DetailQuoteViewController.swift
//  Great Sayings
//
//  Created by Kyryl Nevedrov on 19/04/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class DetailQuoteViewController: UIViewController {
    @IBOutlet weak var quoteLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var authorImageView: UIImageView!
    @IBOutlet weak var bornLabel: UILabel!
    
    var viewModel: DetailQuoteViewModel!
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.tabBarController?.selectedIndex == 2 {
            QuotesService().fetchRandomQuote().subscribe(onNext: { (quote) in
                self.viewModel = DetailQuoteViewModel(quote: quote)
            }).disposed(by: disposeBag)
            
        }
        setupUI()
        bindUI()
    }
    
    private func setupUI() {
    }
    
    private func bindUI() {
        viewModel.quote.subscribe(onNext: {[weak self] (quote) in
            self?.quoteLabel.text = quote.quote
            self?.authorLabel.text = quote.author
            self?.authorImageView.image = UIImage(named: quote.imageName)
            self?.bornLabel.text = quote.authorBornTitle
        }).disposed(by: disposeBag)
    }
    
    @IBAction func actionButtonDidTap(_ sender: Any) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        if let popoverController = alertController.popoverPresentationController {
            popoverController.barButtonItem = sender as? UIBarButtonItem
        }
        let toFavoriteAction = UIAlertAction(title: "Save to favorite", style: .default) { (action) in
            self.viewModel.quote.subscribe(onNext: { (quote) in
                QuotesService().addQuoteToFavorite(quote: quote)
            }).disposed(by: self.disposeBag)
        }
        let shareAction = UIAlertAction(title: "Share", style: .default) { (action) in
            self.viewModel.quote.subscribe(onNext: { (quote) in
                let activityController = UIActivityViewController(activityItems: [UIImage(named: quote.imageName)!, quote.quote], applicationActivities: nil)
                activityController.popoverPresentationController?.sourceView = self.view
                self.present(activityController, animated: true, completion: nil)
            }).disposed(by: self.disposeBag)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(toFavoriteAction)
        alertController.addAction(cancelAction)
        alertController.addAction(shareAction)
        present(alertController, animated: true, completion: nil)
    }
}
