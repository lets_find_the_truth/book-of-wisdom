//
//  SearchViewModel.swift
//  Great Sayings
//
//  Created by Kyryl Nevedrov on 19/04/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import RxSwift

class SearchViewModel {
    private var quotes: Observable<[Quote]>
    var filteredQuotes: Observable<[Quote]>
    var searchText: Variable<String> = Variable("")
    
    init(quoteService: QuotesService) {
        quotes = quoteService.fetchQuotes()
        filteredQuotes = Observable.combineLatest(quotes, searchText.asObservable()) { (quotes, searchText) in
            guard searchText != "" else {
                return quotes
            }
            return quotes.filter { $0.author.contains(searchText) || $0.quote.contains(searchText)}
        }
    }
}
