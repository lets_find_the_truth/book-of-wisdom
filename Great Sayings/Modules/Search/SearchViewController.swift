//
//  SearchViewController.swift
//  Great Sayings
//
//  Created by Kyryl Nevedrov on 19/04/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SearchViewController: UIViewController {
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    
    var viewModel: SearchViewModel = SearchViewModel(quoteService: QuotesService())
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        bindUI()
    }
    
    private func setupUI() {
    }
    
    private func bindUI() {
        searchBar.rx.text.orEmpty.bind(to: viewModel.searchText).disposed(by: disposeBag)
        viewModel.filteredQuotes.bind(to: searchTableView.rx.items(cellIdentifier: "SearchTableViewCell", cellType: SearchTableViewCell.self)) { (row, model, cell) in
            cell.authorImageView.image = UIImage(named: "\(model.imageName)")
            cell.authorLabel.text = model.author
            cell.quoteLabel.text = model.quote
        }.disposed(by: disposeBag)
        
        
        searchTableView.rx.modelSelected(Quote.self)
            .subscribe(onNext: { [weak self](quote) in
                let navController = self?.navigationController
                let detailQuoteViewController = UIStoryboard(name: "Main", bundle: .main).initFromStoryboard(viewController: DetailQuoteViewController.self)
                detailQuoteViewController.viewModel = DetailQuoteViewModel(quote: quote)
                navController?.pushViewController(detailQuoteViewController, animated: true)
            })
            .disposed(by: disposeBag)
    }
    
  

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
