//
//  QuotesListCollectionViewCell.swift
//  Great Sayings
//
//  Created by Kyryl Nevedrov on 22/04/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class QuotesListCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
}
