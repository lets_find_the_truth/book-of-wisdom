//
//  QuotesListViewController.swift
//  Great Sayings
//
//  Created by Kyryl Nevedrov on 19/04/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class QuotesListViewController: UIViewController {
    @IBOutlet weak var collectionViewFlowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK - variables
    private var disposeBag = DisposeBag()
    var viewModel: QuotesListViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.tabBarController?.selectedIndex == 1 {
            viewModel = QuotesListViewModel()
            viewModel.quotes = QuotesService().fetchFavoritesQuotes()
            
        }
        setupUI()
        bindUI()
    }
    
    private func setupUI() {
        let screenSize = UIScreen.main.bounds
        let space: CGFloat = 5
        let numberOfItemsInRow: CGFloat = 3
        let cellWidth = (screenSize.width - space * (numberOfItemsInRow + 1)) / numberOfItemsInRow
        let cellHeight = cellWidth * 1.3
        collectionViewFlowLayout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        collectionViewFlowLayout.minimumLineSpacing = space
        collectionViewFlowLayout.minimumInteritemSpacing = space
        collectionViewFlowLayout.sectionInset = UIEdgeInsets(top: space, left: space, bottom: space, right: space)
    }
    
    private func bindUI() {
        viewModel.quotes.bind(to: collectionView.rx.items(cellIdentifier: "QuotesListCollectionViewCell", cellType: QuotesListCollectionViewCell.self)) { (row, model, cell) in
                cell.imageView.image = UIImage(named: model.imageName)
                cell.titleLabel.text = model.author
            }
            .disposed(by: disposeBag)
        
        collectionView.rx.modelSelected(Quote.self)
            .subscribe(onNext: { [weak self] (quote) in
                let navController = self?.navigationController
                let detailQuoteViewController = UIStoryboard(name: "Main", bundle: .main).initFromStoryboard(viewController: DetailQuoteViewController.self)
                detailQuoteViewController.viewModel = DetailQuoteViewModel(quote: quote)
                navController?.pushViewController(detailQuoteViewController, animated: true)
            })
            .disposed(by: disposeBag)
    }
    
}
