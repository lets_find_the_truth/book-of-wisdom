//
//  QuotesListViewModel.swift
//  Great Sayings
//
//  Created by Kyryl Nevedrov on 19/04/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import RxSwift

class QuotesListViewModel {
    var quotes: Observable<[Quote]>!
    
    init(category: Category) {
        quotes = Observable.of(Array(category.quotes))
    }
    
    init() {
        
    }
}
