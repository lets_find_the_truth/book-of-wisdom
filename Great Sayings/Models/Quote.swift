//
//  Quote.swift
//  Great Sayings
//
//  Created by Kyryl Nevedrov on 19/04/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import RealmSwift

class Quote: Object {
    @objc dynamic var author = ""
    @objc dynamic var quote = ""
    @objc dynamic var authorBornTitle = ""
    @objc dynamic var imageName = ""
    @objc dynamic var isFavorite = false
    @objc dynamic var rating = 0
    
    convenience init(dictionary: [String:Any]) {
        self.init()
        self.author = dictionary["author"] as! String
        self.quote = dictionary["quote"] as! String
        self.authorBornTitle = dictionary["bortTitle"] as! String
        self.imageName = dictionary["imageName"] as! String
        self.isFavorite = dictionary["favorite"] as! Bool
        self.rating = dictionary["rating"] as! Int
    }
}
