//
//  Category.swift
//  Great Sayings
//
//  Created by Kyryl Nevedrov on 22/04/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import RealmSwift

class Category: Object {
    @objc dynamic var title = ""
    @objc dynamic var quote = ""
    @objc dynamic var imageName = ""
    let quotes = List<Quote>()
    
    convenience init(dictionary: [String:Any]) {
        self.init()
        self.title = dictionary["title"] as! String
        self.quote = dictionary["description"] as! String
        self.imageName = dictionary["imageName"] as! String
        for quote in dictionary["items"] as! [[String: Any]] {
            let quote = Quote(dictionary: quote)
            quotes.append(quote)
        }
    }
}

