//
//  QuotesService.swift
//  Great Sayings
//
//  Created by Kyryl Nevedrov on 22/04/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift

final class QuotesService {
    private let realm = try! Realm()
    
    init() {
        guard realm.objects(Category.self).count == 0 else {
            return
        }
        
        if let fileUrl = Bundle.main.url(forResource: "book", withExtension: "json") {
            do {
                let data = try Data(contentsOf: fileUrl)
                let jsonDictionary = try  JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [[String: Any]]
                for item in jsonDictionary {
                    let category = Category(dictionary:item)
                    try! realm.write {
                        realm.add(category)
                    }
                }
            } catch {
                print(error)
            }
        }
        
    }
    
    func fetchCategories() -> Observable<[Category]>{
        let categories = Array(realm.objects(Category.self))
        return Observable.of(categories)
    }
    
    func fetchQuotes() -> Observable<[Quote]>{
        let quotes = Array(realm.objects(Quote.self))
        return Observable.of(quotes)
    }
    
    func fetchRandomQuote() -> Observable<Quote> {
        let filteredQuotes = realm.objects(Quote.self)
        let randomIndex = Int.random(in: 0...filteredQuotes.count)
        return Observable.of(Array(filteredQuotes)[randomIndex])
    }
    
    func fetchFavoritesQuotes() -> Observable<[Quote]> {
        let filteredQuotes = realm.objects(Quote.self).filter {$0.isFavorite == true}
        return Observable.of(Array(filteredQuotes))
    }
    
    func addQuoteToFavorite(quote: Quote) {
        try! realm.write {
            quote.isFavorite = !quote.isFavorite
        }
    }
    
}
